import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    connected: false,
    search: ''
  },
  mutations: {
    connect(state, connected) {
      state.connected = connected;
    },
    search(state, searchVal) {
      state.search = searchVal;
    },
  },
  actions: {

  }
})

