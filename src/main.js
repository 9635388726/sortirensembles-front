import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueSession from 'vue-session'
import { getAxios } from "./getAxios";

Vue.use(VueSession)
Vue.config.productionTip = false

Vue.mixin({
  methods: {
    getAxios
  }
});

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
